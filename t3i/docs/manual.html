<html>
<head>
<link rel="stylesheet" type="text/css" href="docs.css" /> 
<!--
<script type="text/javascript" src="googleprettifier/run_prettify.js"></script>
-->
<script type="text/javascript" src="./jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="./toc.js"></script>

</head>
<body>


<h1>T3i Manual</h1>

<table>
<tr>
<td>
<img src="logo.jpg">
</td>
<td><i>
T3i is a Groovy interactive front-end of T3 to automatically test a Java class.
<br>Author: Wishnu Prasetya
<br>License: GPL v3</i>
</td>
</tr>
</table>

<div id="toc"></div>

T3 is a light weight testing tool to automatically test Java classes. Given
a target class CUT, T3 randomly generates a set of test-sequences, 
called a <i>test suite</i>, against CUT. Each
sequence starts in principle with the creation of an instance of CUT followed 
by calls to the object�s methods, or updates to its fields. T3 can generate a 
large amount of such test sequences to trigger faulty behavior. 

<p>T3i provides a powerful and convenient front-end for T3. Firstly, it allows
us to interactively generate test suites. E.g. we can interactively experiment
with different T3 configurations to generate not just one, but a bunch of test
suites. We write custom value/object generators, or try different variations
to generate test suites. Then, we can combine the generated test suites, 
filter them, and combine them again. With T3i we can also query test suites,
e.g. to check if certain Hoare triples are valid, or class invariants, or
algebraic relations between CUT's methods. We can also use LTL formulas to 
express scenarios, and inspect if they have been covered.

<p><b>How to build and run</b>: See readme.txt.

<!// ================================================================= -->
<h2>A simple example</h2>
<!// ================================================================= -->

Consider this class. Imagine that for whatever purpose we want to test it.

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Sequenic.T3.Examples;

public class Item {
    String name  ;
    int    price ;
    private String code  ;
    
    public Item(String name) {
    	this.name = name ;
    	this.price = 100 ;
    	code = null ;
    }
    
    public void incPrice(int x) { price += x ; }
    public String setCode(String code) { this.code = code; return this.code ; }
    public String getCode() { return code ; }
    public int getPrice() { return price ; }
    public String resetCode() { code = null ; return code ; }
    
}
</pre>
</blockquote>

To test it with T3i, we first run Groovy interpreter, groovysh (putting
interactiveT3.jar in its class path). Once in the interpreter, let's import
stuffs we need from T3i:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
import Sequenic.T3.Config
import Sequenic.T3.Pool
import Sequenic.T3.Sequence.Datatype.SUITE
import Sequenic.T3.Generator.Generator
import static Sequenic.T3.Generator.GenCombinators.FirstOf
import static Sequenic.T3.Generator.Value.ValueMGCombinators.*
import Sequenic.T3.T3groovyAPI
import Sequenic.T3.SuiteUtils.Query.*
import static Sequenic.T3.SuiteUtils.Query.StepPredicate.*
import static Sequenic.T3.SuiteUtils.Query.SeqPredicate.*
import static Sequenic.T3.SuiteUtils.Query.LTLQuery.*
import static Sequenic.T3.SuiteUtils.Query.Alg2Query.*
import static Sequenic.T3.SuiteUtils.Query.Equation.*
import Sequenic.T3.SuiteUtils.SuitePrinter.Printer
</pre>
</blockquote>

Then, let create an instance of T3, and generate a test suite for the class
Item:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> config = new Config(CUT : Sequenic.T3.Examples.Item, maxPrefixLength : 1)
> t3 = new T3groovyAPI(config)
> suite = t3.ADT()
</pre>
</blockquote>

We can generate more suites, e.g. if you
want to experiment with different configurations. Then we ca
add them to make a larger suite; 

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> configb = new Config(CUT : Sequenic.T3.Examples.Item, maxPrefixLength : 4)
> t3b = new T3groovyAPI(configb)
> suite = suite + t3b.ADT() + t3b.ADT() 
</pre>
</blockquote>

Let's now write a Hoare triple, expressing that if x&gt;0 and tobj.price&gt;0,
then after tobj.incPrice(x), tobj.price would be greater than x: 

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> h1 =  hoare({s-> s.args[0]>0 & s.tobj.price>0},"incPrice",{s-> s.tobj.price>s.args[0]})
</pre>
</blockquote>

Let's now query the suite to verify if this specification h1 is valid. We can query
if an LTL (Linear Temporal Logic) formula on a test suite. Such a query will basically
collect the set of all test sequences in the suite, on which the formula holds.
If this set contains all the sequences of the original suite, then the formula is 
universally valid on the entire suite. For the above Hoare triple, we want to check
if it always hold along each step in the sequences (if a step is not
a call to incPrice, the Hoare triple is defined to be valid):

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> ltlquery(S).with(always(h1)).valid()
true
</pre>
</blockquote>

Well, the query says that the formula is valid.
Use print to get more statistics:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> ltlquery(S).with(always(h1)).print()
** Valid
** 144 sequences satisfy the query.
** 0 sequences contradict the query.
</pre>
</blockquote>

As usual, a Hoare triple {p} m(x) {q} is defined implicatively. This means, if a
step is a call to m(x), and if the state before the call satisfies p, then the state
after the call must satisfy q. Notice that the triple is thus trivially valid on a step
that is not a call to m(x), or does not satisfy the pre-condition p. Such a step in
actually not informative for us, thus uninteresting with respect to the Hoare triple
we try to query about. Well, we home that the 144 positive sequences reported above
are not all uninteresting, of course. But how do we know?

<p>h1.antecedent() will give you the pre-condition part of the Hoare triple; it's antecedent.
The following query will count how many sequences have at least one step where the antecedent
holds (thus this step is interesting):

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> ltlquery(S).with(eventually(h1.antecedent())).count()
100
</pre>
</blockquote>

Fortunately, it looks like we have enough real witnesses :) We can collect these 
witness sequences to form a separate test suite:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> suite_for_h1 = ltlquery(S).with(eventually(h1.antecedent())).collect()
</pre>
</blockquote>

Note that the above implies that we can use a query to <b>filter</b> a test suite.
If &phi; is an LTL formula:

<blockquote>
ltlquery(S).with(&phi;).collect()
</blockquote>

will return a subset of the suite S, consisting of all test-sequences on which &phi;
holds.

<h3>Replaying, saving, reloading, and replaying a test suite again</h3>

T3 actually executes the test suite it generates. Once you get the suite you
can always re-execute it (replay it) using an instance of T3, e.g. with
t3 or t3b we created above:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> t3.replay(suite)
</pre>
</blockquote>

Note that the replay-program will use the setting of Config you passed on
when you created the corresponding instance of t3. So far we indeed only
talk about some Config-fields relevant for test suite generation; these
are not relevant for replay. Config does have some fields relevant for
replay, such as replayRunAll, which you can set to true to run all test-sequences
(rather than to stop at the first one that throws an exception). E.g. 
we can do this:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> replayconfig = new Config(replayRunAll : true, replayShowLength = 3)
> t3r = new T3groovyAPI(replayconfig)
> t3r.replay(suite)
</pre>
</blockquote>

You can save the suite, reload it, and replay it again:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> suite.save(".","mysuite",false)  // true would add a timestamp to the suite-name
> T = SUITE.load("mysuite")
> t3r.replay(T)
</pre>
</blockquote>


<!// ================================================================= -->
<h2>Algebraic query</h2>
<!// ================================================================= -->

An algebraic property is a property about how methods of a class interact.
For example, that two methods m1(x) and m2(y) are commutative, which means that 
they do not influence each other. Another example is that the methods k1() and
k2() cancel each other. Such properties are usually expressed as equations, e.g.:

<blockquote>
m1(x) ; m2(y) &nbsp;&nbsp; &equiv; &nbsp;&nbsp; m2(y) ; m1(x) <br>
push(x) ; pop() &nbsp;&nbsp; &equiv; &nbsp;&nbsp; &epsilon;
</blockquote>

Implicitly, we mean the methods to be called on the same instance of CUT. Furthermore,
we probably that it is the final state of this instance that should be compared by
&equiv;. So, if we want to be more explicit, we can write the equations 
e.g. like this:

<blockquote>
tobj.m1(x) ; tobj.m2(y) ; tobj.state()
   &nbsp;&nbsp; = &nbsp;&nbsp; tobj.m2(y) ; tobj.m1(x) ; tobj.state() <br>
tobj.push(x) ; tobj.pop() ; tobj.state()
   &nbsp;&nbsp; = &nbsp;&nbsp; &epsilon ; tobj.state()
</blockquote>

where tobj.state() returns the serialization of tobj, which is comparable with equals
with other serialization. The equation is intended to mean, that if we run the left
hand side (lhs) of the equation on a tobj with state s, the resulting state is 
the same as the resulting state if we would run right hand side (rhs) of the equation on 
the same state s of tobj.

<p>Actually, we can be more general, by allowing tobj to be 
inspected by functions f and g, and to assert the equality on their results instead, 
as in:

<blockquote>
tobj.m1(x) ; tobj.m2(y) ; tobj.f()
   &nbsp;&nbsp; = &nbsp;&nbsp; tobj.m2(y) ; tobj.m1(x) ; tobj.g() <br>
</blockquote>
   
Note that parameters with the same name are meant to refer to the same value.
   
<p>The concrete syntax to express an equation such as above in T3i is shown below, with an example stating
that if we do setCode(x); setCode(y), the value returned by the last step would be
just the same as what we get if we do setCode(y) ; getCode()":   
   
<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> eq = (clause("setCode(x) ; setCode(y)") >> {s-> s.retval}) . equiv(clause("setCode(y) ; getCode()") )
</pre>
</blockquote>

Now to query if the equation is valid in a test suite we do:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> R = algquery(S).with(eq)
> R.valid()
true
</pre>
</blockquote>

Well, it says valid. To get some statistics:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> R.print()
** Valid
** 144 sequences satisfy the query.
** 0 sequences contradict the query.
> R.numOfWitnesses
4
</pre>
</blockquote>

Although the equation is valid, notice that we "only" have four witnesses
for that. A witness is a pair of segments (s,t) in the test suite which
are instances of the lhs and rhs, respectively, and satisfies the asserted
equation above.

<!// ================================================================= -->
<h2>Custom value/object generators</h2>
<!// ================================================================= -->

Consider now the following example, representing triangles. 

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package CUTexamples;
public class Triangle {
	double a = 1 ;
	double b = 2 ;
	double c = 3 ;
	public Triangle() {}
	public void setA(double a) 
	public void setB(double b) 
	public void setC(double c) 
	public boolean isEquilateral() 
	public boolean isIsoleces() 
	public boolean isScalene() 
	public boolean is(String ty) 
}
</pre>
</blockquote>

For example, isEquilateral() will return true if and only if the triangle
has three sides of exactly the same length (we will here ignore details 
about the precision of FP arithmetics). T3 will generate these sides randomly
(through the corresponding setters), but the chance that it accidentally produces
such a triangle will be very very small. We also have the same problem with 
the method "is". It expects a string ty, which has to be one of these: "equilateral", 
"isoleces", or "scalene", and will check if the triangle is of the specified type.
T3's random generator could not have known this; it is thus very unlikely that it can
accidentally produce them.

<p>
While fast, the above describes the typical problem of a random-based testing tool.
It will need your help in the form of a custom object generator. Note that because you
can union test suites, you basically only need to specify a generator to trigger
those cases that T3's standard generator fails to cover.

<p>Here are some example of how to construct a custom Double generator. Analogously we
can construct generators for other primitive types:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
g1 = Double(OneOf(0d,1d,2d))
g2 = Double(OneOf(0d,1d,1d,1d,2d))
</pre>
</blockquote>

OneOf randomly selects one of the specified values. The function "Double" makes sure
that the produced value will only be used to supply a parameter of the corresponding
type (so, it won't be matched to a parameter of type e.g. int).

<p>The following is a string generator, that will only be applied on paramaters whose
name is "ty":

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
g3 = String(OneOf("scalene","equilateral","isoleces")).If (hasParamName("ty"))
</pre>
</blockquote>

We can also combine multiple generators, as below. The resulting generator will 
invoke the first component-generator that succeeds (and fails if none can be found):

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> myGen = FirstOf(
	Double(OneOf(1d,1d,3d)),
	String(OneOf("scalene","equilateral","isoleces")).If (hasParamName("ty")))
</pre>
</blockquote>

So, when a double-parameter is needed, e.g. for the method setA and setB the first
generator will be invoked. For parameters named ty (the method "is" has such one), 
the  second generator will be used.

<p>Finally to use it, we need to pass it when we create an instance of T3:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> config = new Config(CUT:CUTexamples.Triangle, maxPrefixLength:4)
> t3b = new T3groovyAPI(myGen,config)
> suiteb = t3b.ADT()
</pre>
</blockquote>

We can add suiteb to a suite generated using T3's own standard configuration:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> t3 = new T3groovyAPI(config)
> suite = t3.ADT() + suiteb
</pre>
</blockquote>

<h3>Generating Serializable</h3>

The above way of writing custom value generators works for pritive types such as boolean,
int, or String. Generating non-primitive objects takes a bit more effort. A generator
for instances of Serializable can be written in almost the same way:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
g4 = Serializable(MixedOneOf(bob,patrick,octo)).If(hasClass(Person))
</pre>
</blockquote>

where bob, patrick, and octo are instances of Person. This requires the class Person
to implement Serializable.

<h3>Generating Non-Serializable</h3>

A custom generator to generate instances of non-seriazable class C needs to be written
as a static method that takes an int, and returns an instance of C. E.g. as below:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
class NSgen {
   static Email genEmail(int k) {
      switch (k) {
        case 0  : return new Email("root") ;
        case 1  : return new Email("annaz") ;
        default : return new Email("guest") ;
      }
   }
}
</pre>
</blockquote>

However, such a method is not really the representation of a generator that T3 internally
expects. We need to translate it to T3's internal type Generator; here is how:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
g5 = Object(NSgen,"genEmail",OneOf(0,1,1,2)).If(hasClass(Email))
</pre>
</blockquote>

This generator can be passed on to the constructor T3groovyAPI, when we create an
instance of T3, or combined with other generators in a FirstOf construct.



<!// ================================================================= -->
<h2>APIs</h2>
<!// ================================================================= -->

<h3>T3 instance</h3>

To generate test suites you need an instance of T3. Here is how to create one.
The class below also provides utilities to replay and ask general information about
a test suite.

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Sequenic.T3
class T3groovyAPI 
  // create an instance of T3 with the specified configuration:
  T3groovyAPI(Config config) 
  // as above, but passing a reference to a custom object generator:	
  T3groovyAPI(Generator  &lt;PARAM,STEP&gt;  customObjGen, Config config) 
  // run this instance of T3 to generate an ADT suite, respectively non-ADT:
  SUITE ADT() 
  SUITE nonADT() 
  // print the suite's info:
  void info(SUITE S)   
  // replay the suite; uing the configution specified for this instance of T3:
  void replay(SUITE S) 	
</pre>
</blockquote>


<h3>Configuration</h3>

As you have seen, when creating an instance of T3, you can configure it by
passing it an instance of the class Sequenic.T3.Config. Groovy allows you
to create an instance of it, with this convenient syntax:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> config = new Config(fieldName : value, fieldName : value, ...)
</pre>
</blockquote>

Below is the listing of the public fields of Config, along with their default values
if you left them unspecified:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
Class CUT ;
boolean assumeClientInTheSamePackage = true  ;
int maxLevelOfObjectsNesting = 4 ;
int maxCollectionSize = 3 ;
int maxNumberOfStepRetry = 30 ;
int maxNumberOfSeqRetry  = 5 ;
int maxPrefixLength = 8 ;
int maxSuffixLength = 3 ;
/**
 * If true will try to maximize the prefix to the specified maximum length.
 */
boolean maximizePrefix = true ;
double suiteSizeMultiplierPerGoal = 4.0 ;
double fieldUpdateProbability = 0.1 ;
/**
 * If true we will only keep sequences that violates assertions, or throws an exception,
 * one that is caused by a violation to a pre-condition. Other sequences are dropped.
 */
boolean keepOnlyRedTraces = false ;
boolean dropDuplicates = false ;
/**
 * The resulting suite will be split into this number of sub-suites.
 */
int splitSuite = 1 ;
/**
 * An output stream to print out sequence-executions (if requested).
 */
OutputStream sequencePrintOut = System.out ;
/**
 * An output stream to print out report and statistics.
 */
OutputStream reportOut = System.out ;
int reportVerbosity = 1 ;
/**
 *  If true, the CUT is assumed to be correct, and we want to generate
 *  a suite to be used for regression testing. Oralces will be injected
 *  into the sequences; e.g. stating that methods should return exactly
 *  the same values as they do now. If exceptions are thrown, then they
 *  are converted into oracles, stating that the same exceptions should 
 *  be thrown by replay (negative tests).
 */
boolean regressionMode = false ;
/**
 * If regressionMode and this flag are true, this will automatically
 * inject oracles into the generated test-sequence. 
 */
boolean injectOracles = true ;
/**
 * The number of processor cores. 
 */
int numberOfCores = 1 ;
/**
 * A list of paths to roots of the class files that are to be scanned for building
 * an interface map.
 */
String[] dirsToClasses = new String[0] ;
/**
 * If true, when replaying a suite, all sequences will be run; else the execution will 
 * stop at the first step, in the first execution that throws an exception. 
 * This exception will be wrapped in a Violation, and then (re-)thrown.
 */
boolean replayRunAll = false ;
/**
 * If true, then replays that throw exception (or Oracle
 * Error in the regression-mode) will be shown/reported into the given output stream.
 */
boolean replayShowExcExecution = true ;
int replayShowLength = 10 ;
int replayShowDepth = 3 ;
</pre>
</blockquote>

<h3>Suite utilities</h3>

A test suite is an instance of the class SUITE. The most relevant part of its signature
is shown below.

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Sequenic.T3.Sequence.Datatype
class SUITE implements Serializable
  public String CUTname 
  public String timeStamp 
  public String suitename 
  public List<SEQ> suite  
  // return the union of this suite and T2:
  public SUITE plus(SUITE T2) 
  // drop syntatically duplicate sequences
  public void dropDuplicates()
  // save this suite in the dir, with the specified name:
  public void save(String dir, String name, boolean withTimeStamp) throws Exception
  public String showSuiteStatistics() 
  public static SUITE load(String filename) throws Exception
  // Load all suites from the given target directory, that match the given
  // regular expression:
  public static List<SUITE> loadMany(String regexpr, String dir) throws Exception
</pre>
</blockquote>

<h3>Value/object custom generators</h3>

Technically, a generator is an instance of the parameterized type Generator&lt;R,T&gt;.
This type is a Functional Interface. An instance of it can be thought as a 'function'
that takes an object of type R, and produces an object of type T. The intention is to
'generate' an instance of T, and R can be seen as the requirement on the kind of T we
should generate.

<p>Here are some generic combinators, to compose generators into
a more complex one:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Sequenic.T3.Generator
@FunctionalInterface
interface Generator  &lt;Requirement,T&gt;  
  // if p is false on the requirement, the generator fails, else do as this generator:
  public Generator  &lt;Requirement,T&gt;  If (Predicate<Requirement> p)
  // has the specified chance to invoke this generator, and else fails
  public Generator  &lt;Requirement,T&gt;  WithChance (double chance)
  
class GenCombinators 
   public static   &lt;R,T&gt;  Generator  &lt;R,T&gt;  Fail() 
   public static   &lt;R,T&gt;  Generator  &lt;R,T&gt;  FirstOf (Generator  &lt;R,T&gt;  ... gens)
</pre>
</blockquote>

A value/object generator is a more specific instance of Generator, namely
of the type Generator&lt;PARAM,STEP&gt;, where PARAM is an internal representation
containing information about the type of the value or object to generate (such as
its type), and STEP is a meta representation of the generated object. To allow a test
sequence to be replayed, a generated object is not represented directly as the object
itself; but rather as a sequence of instructions of how it is generated. This is stored
in the class STEP. However, this is internal details. The user can simply write
a custom value generator as an instance of type Supplier&lt;T&gt;, where T is the type
of the value to generate, e.g. using the OneOf combinator. See below. We provide bunch of
methods to lift such suppliers to the corresponding Generator&lt;PARAM,STEP&gt;.

<p>Do keep in mind, that custom generator of non-serializable
objects need to follow a certain convention 
shown before (see the section about Custom Generator).


<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Sequenic.T3.Generator.Value
class ValueMGCombinators
	public static Supplier&lt;Integer  &gt; OneOf(int ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  Integer(Supplier  &lt;Integer  &gt; g) 
	public static Supplier  &lt;Boolean  &gt; OneOf(boolean ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  Boolean(Supplier  &lt;Boolean  &gt; g)
	public static Supplier  &lt;Byte  &gt; OneOf(byte ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  Byte(Supplier  &lt;Byte  &gt; g) 
	public static Supplier  &lt;Short  &gt; OneOf(short ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  Short(Supplier  &lt;Short  &gt; g)
	public static Supplier  &lt;Long  &gt; OneOf(long ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  Long(Supplier  &lt;Long  &gt; g) 
	public static Supplier  &lt;Float  &gt; OneOf(float ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  Float(Supplier  &lt;Float  &gt; g) 
	public static Supplier  &lt;Double  &gt; OneOf(double ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  Double(Supplier  &lt;Double  &gt; g)
	public static Supplier  &lt;String  &gt; OneOf(String ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  String(Supplier  &lt;String  &gt; g)
	public static Supplier  &lt;Character  &gt; OneOf(char ... s) 
	public static Generator  &lt;PARAM,STEP&gt;  Char(Supplier  &lt;Character  &gt; g)
	public static Supplier  &lt;Serializable  &gt; MixedOneOf(Serializable ... values) 
	public static Generator  &lt;PARAM,STEP&gt;  Serializable(Supplier  &lt;Serializable  &gt; g) 
	public static Generator  &lt;PARAM,STEP&gt;  Object(Class C, String methodName, Supplier  &lt;Integer  &gt; g) 
	
	public static Predicate  &lt;PARAM  &gt; hasParamName(String name) 
	public static Predicate  &lt;PARAM  &gt; hasClass(Class C) 
	public static Predicate  &lt;PARAM  &gt; isSubclassOf(Class C)
</pre>
</blockquote>

<h3>Query</h3>

A query consists of a query expression &phi;, which is an instance of
Qexpr, and a suite, which is an instance of SUITE. We have two kinds of query expressions,
represented by the subclasses LTL and Equation.

It has this general form:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
ltlquery(S).with(&phi;)     
algquery(S).with(&psi;)
</pre>
</blockquote>

The first is an LTL query, so &phi; has to be an instance of the class LTL. The second
is a query for an algebraic property, so &psi; has to be an instance of Equation. The 
above expressions will <b>execute</b> the requested queries, and the result is 
in principle a subset S+ of S consisting of sequences on which &phi; holds. 
More precisely, the expressions return an instance of the class Query,
which holds the afore mentioned S+, as well as a counter-suite, which is 
S/S+. So, this counter-suite consists of those sequences witnessing the invalidity
of &phi;. Perhaps the name of the class should have been QueryResult :) Anyway,
Query has a number of utility methods to inspect the query-result without having
to re-execute the query itself.

<p>For example, to check if &phi; and &psi; are valid (they hold on all sequences
in the suite):

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
> ltlquery(S).with(&phi;).valid()   
> algquery(S).with(&psi;).valid()   
</pre>
</blockquote>

Here is the relevant part of Query's signature, listing other utilities it has:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Sequenic.T3.SuiteUtils.Query;
class Query 
  public Query with(Qexpr q) // execute a query with q 
  public SUITE collect()     // return sequences on which the query is valid
  public SUITE counter()     // return sequences on which the query is invalid
  public int count()         // return the size of collect()
  public boolean satisfied() // true if collect() is not empty
  public boolean valid()     // true if counter() is not empty
  public void print()        // print some statistics about this query
  public void printSequence(int k)  // print the k-th sequence in collect()      
  public void printCounterSequence(int k) // print the k-th sequence in counter()   
  public void printCounter() // print the first sequence in counter()      
</pre>
</blockquote>

<h3>LTL query expression</h3>

LTL is a subclasss of Qexp, representing LTL formulas. Below are the combinators
available to construct LTL formulas:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Sequenic.T3.SuiteUtils.Query
class LTL extends Qexpr { ... }
class SeqPredicate {
  static LTL now(StepPredicate p) 
  static LTL now(Hoare p) 
  static LTL next(StepPredicate p) 
  static LTL next(Hoare p) 
  static LTL next(LTL p) 
  static LTL not(StepPredicate p) 
  static LTL not(Hoare p) 
  static LTL not(LTL p) 
  static LTL eventually(StepPredicate p) 
  static LTL eventually(Hoare p) 
  static LTL eventually(LTL p) 
  static LTL always(StepPredicate p) 
  static LTL always(Hoare p) 
  static LTL always(LTL p) 	
  static LTL Always(LTL... specs) 	
  static LTL Always(Hoare... specs) 
}
</pre>
</blockquote>

The lowest level of LTL formulas are formed by Hoare triples and step-predicates.
A Hoare triple is actually also a step-predicate, but wrapped a bit more abstractly.
When queried, it will actually be decomposed to a flat step-predicate. These are
combinators to construct Hoare triples:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
package Sequenic.T3.SuiteUtils.Query
class StepPredicate 
  // Hoare triple of a method:
  static Hoare hoare(Predicate&lt;PreState&gt; p, String methodName, Predicate&lt;PostState&gt; q)
  // Hoare triple of a constructor
  static Hoare choare(Predicate&lt;PreState&gt; p, String constrName, Predicate&lt;PostState&gt; q)
		
  public static class Hoare {
    StepPredicate stepPredicate() // translate to a StepPredicate
    StepPredicate antecedent() 
  }
</pre>
</blockquote>

<h3>Pre- and post-conditions</h3>

Pre- and post-conditions are instances of Predicate&lt;PreState&gt respectively Predicate&lt;PostState&gt;.
A Predicate is a Java8 functional interface representing a function from T to bool, for the
specified type-parameter T. We can write a predicate using a &lambda;-expression. Since you use
T3i in Groovy, we will use the Groovy syntax. So examples of pre- and post-conditions:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
p = {s-> s.args[0]>0 & s.tobj.price>0}  // &lambda;-expression
q = {s-> s.tobj.price>s.args[0]}        // &lambda;-expression
h1 = hoare(p,"incPrice",q) 
</pre>
</blockquote>

<h3>Algebraic query expression</h3> 

An algebraic query expression is represented by the class Equation, which is
a subclass of Qexpr. An instance of Equation is constructed with this syntax:

<blockquote class="PROGRAMLISTING">
<pre  class="prettyprint ">
clause(lhs >> f) . equiv(clause(rhs >> g))
clause(lhs >> f) . equiv(clause(rhs)
clause(lhs >> f) . equiv(Epsilon)
</pre>
</blockquote>

lhs and rhs are strings representing the left and right hands side of the equation.
f and g are instances of Function&lt;PostState,Object&rt;. Such an equation
assert that if lhs is executed in the state s of an instance of CUT, and then
we evaluate f on the resulting state; the result is the same as if we execute
rhs on s, and then evaluate g on the resulting state.

<p>If g is ommitted, it is assumed to be the same as f. Epsilon stands for an empty
side.

The syntax of lhs and rhs is as follows:

<blockquote class="PROGRAMLISTING">
clause  ::=  step ( ; step)* <br>
step    ::=  methodName() | methodName(params) <br>
params  ::=  paramName (, paramName)*  
</blockquote>

For example, as in:

<blockquote class="PROGRAMLISTING">
eq = (        clause("setCode(x) ; setCode(y)") >> {s-> s.retval}) 
      . equiv(clause("setCode(y) ; getCode()") )
</blockquote>

Parameters with same name are interpreted to bound to the same value.


<script type="text/javascript">
  $('#toc').toc({
    'selectors': 'h2,h3'}) ;
</script>
</body>
</html>
